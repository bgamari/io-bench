module Main where

import System.Environment (getArgs)
import System.Clock
import System.IO
import Foreign.Marshal.Alloc
import GHC.IO.Handle.Text
import Control.Monad
import Data.Maybe

main :: IO ()
main = do
    [bufSizeStr] <- getArgs
    let bufSize = read bufSizeStr
    --worker (kilobyte bufSize) "input" (Just "output")
    worker (kilobyte bufSize) "input" Nothing

kilobyte :: Int -> Int
kilobyte = (* 1024)

megabyte :: Int -> Int
megabyte = (* 1024^2)

gigabyte :: Int -> Int
gigabyte = (* 1024^3)

worker :: Int -> FilePath -> Maybe FilePath -> IO ()
worker chunk_size inp outp = do
    inp_h <- openFile inp ReadMode
    outp_h <- mapM (flip openFile WriteMode) outp
    sz <- hFileSize inp_h
    t <- loop chunk_size inp_h outp_h
    print $ realToFrac sz / (1e-9 * realToFrac t) / realToFrac (megabyte 1)

timeIt :: IO a -> IO (Integer, a)
timeIt action = do
    t0 <- getTime Monotonic
    r <- action
    t1 <- getTime Monotonic
    let dt = toNanoSecs $ t1 `diffTimeSpec` t0
    return (dt, r)
    
loop :: Int -> Handle -> Maybe Handle -> IO Integer
loop chunk_size inp outp = do
    (dt, _) <- timeIt $ allocaBytes chunk_size go
    return dt
  where
    go buf = do
        n <- hGetBuf inp buf chunk_size
        if n > 0
          then do
            mapM (\h -> hPutBuf h buf n) outp
            when (n > 0) (go buf)
          else return ()
