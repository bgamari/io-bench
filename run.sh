#!/usr/bin/env bash
set -e

GHC="${GHC:-ghc}"
input_sz="$((10*1024*1024*1024))"

make_input() {
    yes "hello world" | head -c $input_sz > input
}

if [[ ! -f input ]]; then
    make_input
fi

run_one() {
    buf_size="16"
    rm -R dist-newstyle
    cabal build -w $GHC io-bench
    mkdir -p "$OUT_DIR"
    exe="$(cabal list-bin -w $GHC io-bench)"
    /usr/bin/env time -o "$OUT_DIR/time" \
        "$exe" "$buf_size" +RTS "-s$OUT_DIR/rts" > "$OUT_DIR/out"
    rm -f output
}

compare() {
    GHC1="$1"
    GHC2="$2"
    make_input
    mkdir -p runs.out
    for i in $(seq 10); do
        echo "Run $i..."
        GHC="$GHC1" OUT_DIR="runs.out/ghc1/$i" run_one
        GHC="$GHC2" OUT_DIR="runs.out/ghc2/$i" run_one
    done
}

compare $@
